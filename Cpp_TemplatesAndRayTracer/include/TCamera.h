#pragma once
#include "TPoint.h"
#include "TVect3D.h"

template <typename T>
class TCamera
{
	static_assert(std::is_pod<T>::value);
public:
	TCamera(const TPoint<T>& position = TPoint<T>(), const TVect3D<T>& direction = TVect3D<T>(1, 0, 0), const TVect3D<T>& right = TVect3D<T>(0, 0, 0), const TVect3D<T>& down = TVect3D<T>(0, 0, 0)) 
		: m_position(position), m_direction(direction), m_right(right), m_down(down)
	{
	}

	TCamera(const TCamera& camera)
	{
		m_position = camera.m_position;
		m_direction = camera.m_direction;
		m_right = camera.m_right;
		m_down = camera.m_down;
	}

	TCamera(TCamera<T>&& camera) = default;
	~TCamera() = default;
	TCamera<T>& operator=(const TCamera<T>& camera) = default;
	TCamera<T>& operator=(TCamera<T> && camera) = default;

	TPoint<T> GetPosition() const
	{
		return m_position;
	}
	
	TVect3D<T> GetDirection() const
	{
		return m_direction;
	}

	TVect3D<T> GetRight() const
	{
		return m_right;
	}

	TVect3D<T> GetDown() const
	{
		return m_down;
	}

	void SetPosition(const TPoint<T>& position)
	{
		m_position = position;
	}

	void SetDirection(const TVect3D<T>& direction)
	{
		m_direction = direction;
	}

	void SetRight(const TVect3D<T>& right)
	{
		m_right = right;
	}
	void SetDown(const TVect3D<T>& down)
	{
		m_down = down;
	}

	friend std::ostream& operator<<(std::ostream& os, TCamera<T> const& camera);

private:
	TPoint<T> m_position;
	TVect3D<T> m_direction;
	TVect3D<T> m_right;
	TVect3D<T> m_down;
};

std::ostream& operator<<(std::ostream& os, TCamera<int> const& camera)
{
	os << "Position : " << camera.m_position << " - Direction : " << camera.m_direction << " - Right Side : " << camera.m_right << " - Up Side : " << camera.m_down;
	return os;
}

std::ostream& operator<<(std::ostream& os, TCamera<double> const& camera)
{
	os << "Position : " << camera.m_position << " - Direction : " << camera.m_direction << " - Right Side : " << camera.m_right << " - Up Side : " << camera.m_down;
	return os;
}