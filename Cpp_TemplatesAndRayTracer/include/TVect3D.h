#pragma once
#include <cmath>
#include "TPoint.h"

template <typename T>
class TVect3D
{
	static_assert(std::is_pod<T>::value);

private:
	T m_x;
	T m_y;
	T m_z;

public:
	TVect3D(const T& x = 0, const T& y = 0, const T& z = 0) : m_x(x), m_y(y), m_z(z)
	{
	}

	template <typename G>
	TVect3D(const TPoint<G>& pt1, const TPoint<G>& pt2)
	{
		m_x = pt2.GetX() - pt1.GetX();
		m_y = pt2.GetY() - pt1.GetY();
		m_z = pt2.GetZ() - pt1.GetZ();
	}

	TVect3D(const TVect3D& vect)
	{
		m_x = vect.m_x;
		m_y = vect.m_y;
		m_z = vect.m_z;
	}

	~TVect3D() = default;

	T GetX() const
	{
		return m_x;
	}

	T GetY() const
	{
		return m_y;
	}

	T GetZ() const
	{
		return m_z;
	}

	double GetLength() const
	{
		return sqrt(pow(m_x, 2) + pow(m_y, 2) + pow(m_z, 2));
	}

	TVect3D<T> GetNormalized() const
	{
		const auto length = GetLength();
		TVect3D<T> result(m_x / length, m_y / length, m_z / length);
		return result;
	}

	TVect3D GetOpposite() const
	{
		return { -m_x, -m_y, -m_z };
	}

	double DotProduct(const	TVect3D<T>& vectA) const
	{
		return m_x * vectA.m_x + m_y * vectA.m_y + m_z * vectA.m_z;
	}

	void SetX(const T& x)
	{
		m_x = x;
	}

	void SetY(const T& y)
	{
		m_y = y;
	}

	void SetZ(const T& z)
	{
		m_z = z;
	}

	void SeTToNull()
	{
		m_x = 0;
		m_y = 0;
		m_z = 0;
	}

	TVect3D& operator+=(const TVect3D& vectA)
	{
		m_x += vectA.m_x;
		m_y += vectA.m_y;
		m_z += vectA.m_z;
		return *this;
	}

	TVect3D& operator-=(const TVect3D& vectA)
	{
		m_x -= vectA.m_x;
		m_y -= vectA.m_y;
		m_z -= vectA.m_z;
		return *this;
	}

	TVect3D& operator*=(const TVect3D& vectA)
	{
		TVect3D<T> save(*this);
		m_x = save.m_y * vectA.m_z - save.m_z * vectA.m_y;
		m_y = save.m_z * vectA.m_x - save.m_x * vectA.m_z;
		m_z = save.m_x * vectA.m_y - save.m_y * vectA.m_x;
		return *this;
	}

	TVect3D& operator*=(const T& val)
	{
		m_x *= val;
		m_y *= val;
		m_z *= val;
		return *this;
	}

	TVect3D& operator/=(const TVect3D& vectA)
	{
		m_x /= vectA.m_x;
		m_y /= vectA.m_y;
		m_z /= vectA.m_z;
		return *this;
	}

	TVect3D& operator/=(const T& val)
	{
		m_x /= val;
		m_y /= val;
		m_z /= val;
		return *this;
	}

	friend bool operator==(TVect3D<T> const& vectA, TVect3D<T> const& vectB);
	friend std::ostream& operator<<(std::ostream& os, TVect3D<T> const& vect);
};

bool operator==(TVect3D<int> const& vectA, TVect3D<int> const& vectB)
{
	return (vectA.m_x == vectB.m_x && vectA.m_y == vectB.m_y && vectA.m_z == vectB.m_z);
}

bool operator==(TVect3D<double> const& vectA, TVect3D<double> const& vectB)
{
	return (vectA.m_x == vectB.m_x && vectA.m_y == vectB.m_y && vectA.m_z == vectB.m_z);
}

template <typename T>
bool operator!=(TVect3D<T> const& vectA, TVect3D<T> const& vectB)
{
	return !(vectA == vectB);
}

template <typename T>
TPoint<T> operator+(TPoint<T> const& pt, TVect3D<T> const& vectA)
{
	return TPoint<T>(vectA.GetX() + pt.GetX(), vectA.GetY() + pt.GetY(), vectA.GetZ() + pt.GetZ());
}

template <typename T>
TPoint<T> operator-(TPoint<T> const& pt, TVect3D<T> const& vectA)
{
	return TPoint<T>(vectA.GetX() - pt.GetX(), vectA.GetY() - pt.GetY(), vectA.GetZ() - pt.GetZ());
}

template <typename T>
TVect3D<T> operator+(TVect3D<T> const& vectA, TVect3D<T> const& vectB)
{
	TVect3D<T> resultat(vectA);
	resultat += vectB;
	return resultat;
}

template <typename T>
TVect3D<T> operator-(TVect3D<T> const& vectA, TVect3D<T> const& vectB)
{
	TVect3D<T> resultat(vectA);
	resultat -= vectB;
	return resultat;
}

template <typename T>
TVect3D<T> operator*(TVect3D<T> const& vectA, TVect3D<T> const& vectB)
{
	TVect3D<T> resultat(vectA);
	resultat *= vectB;
	return resultat;
}

template <typename T, typename G>
TVect3D<T> operator*(TVect3D<T> const& vectA, G const& val)
{
	TVect3D<T> resultat(vectA);
	resultat *= val;
	return resultat;
}

template <typename T, typename G>
TVect3D<T> operator*(G const& val, TVect3D<T> const& vectA)
{
	TVect3D<T> resultat(vectA);
	resultat *= val;
	return resultat;
}

template <typename T>
TVect3D<T> operator/(TVect3D<T> const& vectA, TVect3D<T> const& vectB)
{
	TVect3D<T> resultat(vectA);
	resultat /= vectB;
	return resultat;
}

template <typename T, typename G>
TVect3D<T> operator/(TVect3D<T> const& vectA, G const& val)
{
	TVect3D<T> resultat(vectA);
	resultat /= val;
	return resultat;
}

std::ostream& operator<<(std::ostream& os, TVect3D<int> const& vect)
{
	os << '[' << vect.m_x << ',' << vect.m_y << ',' << vect.m_z << ']';
	return os;
}

std::ostream& operator<<(std::ostream& os, TVect3D<double> const& vect)
{
	os << '[' << vect.m_x << ',' << vect.m_y << ',' << vect.m_z << ']';
	return os;
}