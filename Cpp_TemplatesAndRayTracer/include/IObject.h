#pragma once
#include "TVect3D.h"
#include "TPoint.h"
#include "TRay.h"
#include "Color.h"

template<typename T>
class IObject
{
	static_assert(std::is_pod<T>::value);

public:
	~IObject() = default;

	virtual Color GetColorAtPoint(const TPoint<T>& pt) const = 0;
	virtual TVect3D<T> GetNormalAtPoint(const TPoint<T>& pt) const = 0;
	virtual double FindIntersection(const TRay<T>& _ray) = 0;
};