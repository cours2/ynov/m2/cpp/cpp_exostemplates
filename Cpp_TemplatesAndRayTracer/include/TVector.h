#pragma once

#include "TVect3D.h"
#include <iostream>

template <typename T> 
class TVector
{
private:
	T* m_data;
	int m_size;
	int m_current;

public:
	TVector(const int& size = 1) : m_data(nullptr), m_size(size), m_current(0)
	{
		if (size <= 0) {
			return;
		}
		else {
			m_data = new T[size];
		}
	}

	TVector(const TVector<T>& vector) : m_data(nullptr), m_size(vector.m_size), m_current(vector.m_current)
	{
		m_data = new T[vector.m_size];
	}

	~TVector()
	{
		m_size = 0;
		m_current = 0;
		delete m_data;
		m_data = nullptr;
	}

	void PushBack(T val)
	{
		if (m_current == m_size) {
			T* tmp = new T[2 * m_size];
			for (int i = 0; i < m_size; i++) {
				tmp[i] = m_data[i];
			}
			delete[] m_data;
			m_size *= 2;
			m_data = tmp;
		}
		m_data[m_current] = val;
		m_current++;
	}

	void PushAt(T val, int index)
	{
		if (index == m_size) {
			PushBack(val);
		}
		else {
			m_data[index] = val;
		}
	}

	T Get(int at)
	{
		if (at < m_current) {
			return m_data[at];
		}
	}

	void Pop()
	{ 
		--m_current;
	}
	
	int Current() 
	{ 
		return m_current;
	}

	int Size()
	{ 
		return m_size;
	}

	friend std::ostream& operator<<(std::ostream& os, TVector<T> const& vector);
};

std::ostream& operator<<(std::ostream& os, TVector<int> const& vector) {
	for (int a = 0; a < vector.m_current; ++a) {
		os << vector.m_data[a] << " ";
	}
	return os;
}

std::ostream& operator<<(std::ostream& os, TVector<double> const& vector) {
	for (int a = 0; a < vector.m_current; ++a) {
		os << vector.m_data[a] << " ";
	}
	return os;
}

std::ostream& operator<<(std::ostream& os, TVector<TVect3D<int>> const& vector) {
	for (int a = 0; a < vector.m_current; ++a) {
		os << vector.m_data[a] << " ";
	}
	return os;
}

std::ostream& operator<<(std::ostream& os, TVector<TVect3D<double>> const& vector) {
	for (int a = 0; a < vector.m_current; ++a) {
		os << vector.m_data[a] << " ";
	}
	return os;
}

std::ostream& operator<<(std::ostream& os, TVector<TPoint<int>> const& vector) {
	for (int a = 0; a < vector.m_current; ++a) {
		os << vector.m_data[a] << " ";
	}
	return os;
}

std::ostream& operator<<(std::ostream& os, TVector<TPoint<double>> const& vector) {
	for (int a = 0; a < vector.m_current; ++a) {
		os << vector.m_data[a] << " ";
	}
	return os;
}