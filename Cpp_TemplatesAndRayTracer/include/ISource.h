#pragma once
#include "TPoint.h"
#include "Color.h"

template<typename T>
class ISource
{
	static_assert(std::is_pod<T>::value);

public:
	~ISource() = default;

	virtual Color GetColor() const = 0;
	virtual TPoint<T> GetPosition() const = 0;
	virtual void SetColor(const Color& color) = 0;
	virtual void SetPosition(const TPoint<T>& pos) = 0;
};