#pragma once
#include <iostream>

template <typename T>
class TPoint
{
	static_assert(std::is_pod<T>::value);
public:
	TPoint(const T& x = 0, const T& y = 0, const T& z = 0) : m_x(x), m_y(y), m_z(z)
	{
	}

	TPoint(const TPoint& pt)
	{
		m_x = pt.m_x;
		m_y = pt.m_y;
		m_z = pt.m_z;
	}

	~TPoint() = default;

	T GetX() const
	{
		return m_x;
	}

	T GetY() const
	{
		return m_y;
	}

	T GetZ() const
	{
		return m_z;
	}

	void SetX(const T& x)
	{
		m_x = x;
	}

	void SetY(const T& y)
	{
		m_y = y;
	}

	void SetZ(const T& z)
	{
		m_z = z;
	}

	void SetPointToOrigin()
	{
		m_x = 0;
		m_y = 0;
		m_z = 0;
	}

	friend std::ostream& operator<<(std::ostream& os, TPoint<T> const& pt);

private:
	T m_x;
	T m_y;
	T m_z;
};

std::ostream& operator<<(std::ostream& os, TPoint<int> const& pt)
{
	os << '(' << pt.m_x << ',' << pt.m_y << ',' << pt.m_z << ')';
	return os;
}

std::ostream& operator<<(std::ostream& os, TPoint<double> const& pt)
{
	os << '(' << pt.m_x << ',' << pt.m_y << ',' << pt.m_z << ')';
	return os;
}