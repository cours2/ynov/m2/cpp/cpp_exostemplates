#pragma once
// Find this code at : https://www.geeksforgeeks.org/quickhull-algorithm-convex-hull/
// I have to update it so it can work with my implementation of TPoint and TVector !


// C++ program to implement Quick Hull algorithm 
// to find convex hull. 
#include<iostream> 
#include <cmath>
#include <vector>
#include "TPoint.h"
using namespace std;


// Stores the result (points of convex hull) 
vector<TPoint<int>> hull;

// Returns the side of point p with respect to line 
// joining points p1 and p2. 
int findSide(TPoint<int> p1, TPoint<int> p2, TPoint<int> p)
{
	int val = (p.GetY() - p1.GetY()) * (p2.GetX() - p1.GetX()) -
		(p2.GetY() - p1.GetY()) * (p.GetX() - p1.GetX());

	if (val > 0)
		return 1;
	if (val < 0)
		return -1;
	return 0;
}

// returns a value proportional to the distance 
// between the point p and the line joining the 
// points p1 and p2 
int lineDist(TPoint<int> p1, TPoint<int> p2, TPoint<int> p)
{
	return abs((p.GetY() - p1.GetY()) * (p2.GetX() - p1.GetX()) -
		(p2.GetY() - p1.GetY()) * (p.GetX() - p1.GetX()));
}

// End points of line L are p1 and p2. side can have value 
// 1 or -1 specifying each of the parts made by the line L 
void quickHull(TPoint<int> a[], int n, TPoint<int> p1, TPoint<int> p2, int side)
{
	int ind = -1;
	int max_dist = 0;

	// finding the point with maximum distance 
	// from L and also on the specified side of L. 
	for (int i = 0; i < n; i++)
	{
		int temp = lineDist(p1, p2, a[i]);
		if (findSide(p1, p2, a[i]) == side && temp > max_dist)
		{
			ind = i;
			max_dist = temp;
		}
	}

	// If no point is found, add the end points 
	// of L to the convex hull. 
	if (ind == -1)
	{
		hull.push_back(p1);
		hull.push_back(p2);
		return;
	}

	// Recur for the two parts divided by a[ind] 
	quickHull(a, n, a[ind], p1, -findSide(a[ind], p1, p2));
	quickHull(a, n, a[ind], p2, -findSide(a[ind], p2, p1));
}

void printHull(TPoint<int> a[], int n)
{
	// a[i].second -> y-coordinate of the ith point 
	if (n < 3)
	{
		cout << "Convex hull not possible\n";
		return;
	}

	// Finding the point with minimum and 
	// maximum x-coordinate 
	int min_x = 0, max_x = 0;
	for (int i = 1; i < n; i++)
	{
		if (a[i].GetX() < a[min_x].GetX())
			min_x = i;
		if (a[i].GetX() > a[max_x].GetX())
			max_x = i;
	}

	// Recursively find convex hull points on 
	// one side of line joining a[min_x] and 
	// a[max_x] 
	quickHull(a, n, a[min_x], a[max_x], 1);

	// Recursively find convex hull points on 
	// other side of line joining a[min_x] and 
	// a[max_x] 
	quickHull(a, n, a[min_x], a[max_x], -1);

	cout << "The points in Convex Hull are:\n";
	while (!hull.empty())
	{
		cout << "(" << (*hull.begin()).GetX() << ", "
			<< (*hull.begin()).GetY() << ") ";
		hull.erase(hull.begin());
	}
}