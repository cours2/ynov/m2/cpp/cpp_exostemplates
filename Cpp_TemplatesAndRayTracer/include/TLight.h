#pragma once

#include "TPoint.h"
#include "Color.h"
#include "ISource.h"

#include <iostream>

template <typename T>
class TLight : public ISource<T>
{
	static_assert(std::is_pod<T>::value);
public:
	TLight(const TPoint<T>& pos = (0,0,0), const Color& color = (0,0,0,0)) : m_pos(pos), m_color(color)
	{
	}

	TLight(const TLight& light)
	{
		m_pos = light.m_pos;
		m_color = light.m_color;
	}

	~TLight() = default;

	virtual TPoint<T> GetPosition() const override
	{
		return m_pos;
	}

	virtual Color GetColor() const override
	{
		return m_color;
	}

	void SetPosition(const TPoint<T>& pos) override
	{
		m_pos = pos;
	}

	void SetColor(const Color& color) override
	{
		m_color = color;
	}

	friend std::ostream& operator<<(std::ostream& os, TLight<T> const& light);

private:
	TPoint<T> m_pos;
	Color m_color;
};

std::ostream& operator<<(std::ostream& os, TLight<int> const& light)
{
	os << "Position : " << light.m_pos << ", Color : " << light.m_color;
	return os;
}

std::ostream& operator<<(std::ostream& os, TLight<double> const& light)
{
	os << "Position : " << light.m_pos << ", Color : " << light.m_color;
	return os;
}