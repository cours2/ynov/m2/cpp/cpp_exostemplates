#pragma once
#include "IObject.h"
#include "TVect3D.h"
#include "TPoint.h"
#include "Color.h"

template <typename T, typename G = T>
class TSphere : public IObject<T>
{
	static_assert(std::is_pod<T>::value);
	static_assert(std::is_pod<G>::value);
public:
	TSphere(const TPoint<T>& origin = TPoint<T>(), const G& radius = 1, const Color& color = Color(0.5, 0.5, 0.5, 0.5)) : m_origin(origin), m_radius(radius), m_color(color)
	{
	}

	TSphere(const TSphere& sphere)
	{
		m_origin = sphere.m_origin;
		m_radius = sphere.m_radius;
		m_color = sphere.m_color;
	}

	~TSphere() = default;

	TPoint<T> GetOrigin() const
	{
		return m_origin;
	}

	G GetRadius() const
	{
		return m_radius;
	}
	
	Color GetColor() const
	{
		return m_color;
	}

	void SetOrigin(const TPoint<T>& pt)
	{
		m_origin = pt;
	}

	void SetRadius(const G& radius)
	{
		m_radius = radius;
	}

	void SetColor(const Color& color)
	{
		m_color = color;
	}

	Color GetColorAtPoint(const TPoint<T>& pt) const override
	{
		return m_color;
	}


	TVect3D<T> GetNormalAtPoint(const TPoint<T>& pt) const override
	{
		return TVect3D<T>(m_origin, pt).GetNormalized();
	}

	double FindIntersection(const TRay<T>& ray) override
	{
		// TODO 1) First thing to do !
		// Write Sphere intersection algorithm.
		// Do not forget that if there is no intersection you have to return -1
		const TVect3D<T> vectOriginCenter = TVect3D<T>(m_origin, ray.GetOrigin());
		double a = ray.GetDirection().DotProduct(ray.GetDirection());
		double b = 2.0 * vectOriginCenter.DotProduct(ray.GetDirection());
		double c = vectOriginCenter.DotProduct(vectOriginCenter) - m_radius * m_radius;
		double discriminant = b * b - 4 * a * c;
		if (discriminant < 0)
		{
			return -1.0;
		}
		else
		{
			return (-b - sqrt(discriminant)) / (2.0 * a);
		}
	}

	friend std::ostream& operator<<(std::ostream& os, TSphere<T, G> const& sphere);

private:
	TPoint<T> m_origin;
	G m_radius;
	Color m_color;
};

std::ostream& operator<<(std::ostream& os, TSphere<int, int> const& sphere)
{
	os << "Origin : " << sphere.m_origin << " - Radius : " << sphere.m_radius << " - Color : " << sphere.m_color;
	return os;
}

std::ostream& operator<<(std::ostream& os, TSphere<int, double> const& sphere)
{
	os << "Origin : " << sphere.m_origin << " - Radius : " << sphere.m_radius << " - Color : " << sphere.m_color;
	return os;
}

std::ostream& operator<<(std::ostream& os, TSphere<double, int> const& sphere)
{
	os << "Origin : " << sphere.m_origin << " - Radius : " << sphere.m_radius;
	return os;
}

std::ostream& operator<<(std::ostream& os, TSphere<double, double> const& sphere)
{
	os << "Origin : " << sphere.m_origin << " - Radius : " << sphere.m_radius;
	return os;
}