#pragma once

#include "TVect3D.h"
#include <iostream>

template <typename T> 
struct Node
{
	T m_value;
	Node* m_before;
	Node* m_after;

	Node() : m_before(nullptr), m_after(nullptr)
	{
	}

	~Node()
	{
		if (m_before != nullptr) {
			delete m_before;
			m_before = nullptr;
		}

		if (m_after != nullptr) {
			delete m_after;
			m_after = nullptr;
		}
	}
};

template <typename T>
class TList
{
private:
	int m_size;
	Node<T>* m_start;
	Node<T>* m_end;

public:
	TList() : m_size(0), m_start(nullptr), m_end(nullptr)
	{
	}

	~TList()
	{
		if (m_start) {
			delete m_start;
			m_start = nullptr;
		}

		if (m_end) {
			delete m_end;
			m_end = nullptr;
		}
	}

	void PushBack(const T& val)
	{
		Node<T>* newNode = new Node<T>();
		newNode->m_value = val;
		if (m_start == nullptr && m_size == 0) {
			newNode->m_before = m_start;
			newNode->m_after = m_end;
			m_start = newNode;
		}
		else {
			newNode->m_before = m_end;
			newNode->m_after = nullptr;
			m_end->m_after = newNode;
		}
		m_end = newNode;
		++m_size;
	}
	
	void Insert(const T& val, const int& at = -1)
	{
		if (at >= m_size || at < 0 || (m_start == nullptr && m_size == 0)) {
			PushBack(val);
		}
		else {
			Node<T>* newNode = new Node<T>();
			newNode->m_value = val;
			Node<T>* tmp = m_start;
			for (int a = 0; a < at; ++a) {
				tmp = tmp->m_after;
			}
			newNode->m_before = tmp->m_before;
			newNode->m_after = tmp;
			tmp->m_before = newNode;
			if (newNode->m_before != nullptr) {
				newNode->m_before->m_after = newNode;
			}
			else {
				m_start = newNode;
			}
			++m_size;
		}
	}

	void PushFront(const T& val)
	{
		Insert(val, 0);
	}

	void popBack()
	{
		if (m_end != nullptr) {
			m_end = m_end->m_before;
			if (m_end != nullptr) {
				m_end->m_after = nullptr;
			}
			--m_size;
		}
	}

	T Front() {
		if (m_start != nullptr && m_size != 0) {
			return m_start->m_value;
		}
		else {
			std::cout << "list empty, no front found";
			return NULL;
		}
	}
	
	T Back() {
		if (m_end != nullptr && m_size != 0) {
			return m_end->m_value;
		}
		else {
			std::cout << "list empty, no back found";
			return NULL;
		}
	}

	bool Empty() {
		return m_size == 0;
	}

	int Size() {
		return m_size;
	}

	void Clear() {
		m_start = nullptr;
		m_end = nullptr;
		m_size = 0;
	}

	friend std::ostream& operator<<(std::ostream& os, TList<T> const& list);
};

std::ostream& operator<<(std::ostream& os, TList<int> const& list)
{
	if (list.m_size == 0) {
		os << "List empty.";
	}
	else {
		Node<int>* node = list.m_start;
		os << node->m_value << "   ";
		while (node->m_after != nullptr) {
			node = node->m_after;
			os << node->m_value << "   ";;
		}
	}
	return os;
}

std::ostream& operator<<(std::ostream& os, TList<double> const& list)
{
	if (list.m_size == 0) {
		os << "List empty.";
	}
	else {
		Node<double>* node = list.m_start;
		os << node->m_value << "   ";
		while (node->m_after != nullptr) {
			node = node->m_after;
			os << node->m_value << "  ";;
		}
	}
	return os;
}