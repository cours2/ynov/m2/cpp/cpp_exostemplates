#pragma once
#include "TVect3D.h"
#include "TPoint.h"

template <typename T, typename G = T>
class TRay
{
	static_assert(std::is_pod<T>::value);
	static_assert(std::is_pod<G>::value);
public:
	TRay(const TPoint<T>& origin = TPoint<T>(), const TVect3D<G>& direction = TVect3D<G>(1, 1, 1)) : m_origin(origin), m_direction(direction)
	{
	}

	TRay(const TRay<T, G>& ray)
	{
		m_origin = ray.m_origin;
		m_direction = ray.m_direction;
	}

	~TRay() = default;

	TPoint<T> GetOrigin() const
	{
		return m_origin;
	}

	TVect3D<G> GetDirection() const
	{
		return m_direction;
	}

	void SetOrigin(const TPoint<T>& pt)
	{
		m_origin = pt;
	}

	void SetDirection(const TVect3D<G>& vect)
	{
		m_direction = vect;
	}

	friend std::ostream& operator<<(std::ostream& os, TRay<T, G> const& ray);

private:
	TPoint<T> m_origin;
	TVect3D<G> m_direction;
};

std::ostream& operator<<(std::ostream& os, TRay<int, int> const& ray)
{
	os << "Origin : " << ray.m_origin << " - Direction : " << ray.m_direction;
	return os;
}

std::ostream& operator<<(std::ostream& os, TRay<int, double> const& ray)
{
	os << "Origin : " << ray.m_origin << " - Direction : " << ray.m_direction;
	return os;
}

std::ostream& operator<<(std::ostream& os, TRay<double, int> const& ray)
{
	os << "Origin : " << ray.m_origin << " - Direction : " << ray.m_direction;
	return os;
}

std::ostream& operator<<(std::ostream& os, TRay<double, double> const& ray)
{
	os << "Origin : " << ray.m_origin << " - Direction : " << ray.m_direction;
	return os;
}