#pragma once
#include "IObject.h"
#include "TVect3D.h"
#include "Color.h"

template <typename T>
class TPlane : public IObject<T>
{
	static_assert(std::is_pod<T>::value);
public:
	TPlane(const TVect3D<T>& normal = TVect3D<T>(1,0,0), const T& distance = 0, const Color& color = Color(0.5, 0.5, 0.5, 0.5)) : m_normal(normal), m_distance(distance), m_color(color)
	{
	}

	TPlane(const TPlane& plane)
	{
		m_normal = plane.m_normal;
		m_distance = plane.m_distance;
		m_color = plane.m_color;
	}

	~TPlane() = default;

	TVect3D<T> GetNormal() const
	{
		return m_normal;
	}

	Color GetColor() const
	{
		return m_color;
	}

	T GetDistance() const
	{
		return m_distance;
	}

	void SetNormal(const TVect3D<T>& normal)
	{
		m_normal = normal;
	}

	void SetColor(const Color& color)
	{
		m_color = color;
	}

	void SetDistance(const T& distance)
	{
		m_distance = distance;
	}

	Color GetColorAtPoint(const TPoint<T>& pt) const override
	{
		if (m_color.Alpha() == 2.0)
		{
			const auto square = static_cast<int>(std::floor(pt.GetX())) + static_cast<int>(std::floor(pt.GetZ()));
			if ((square % 2) == 0)
				return Color(0.0, 0.0, 0.0, m_color.Alpha());

			return Color(1.0, 1.0, 1.0, m_color.Alpha());
		}
		return m_color;
	}


	TVect3D<T> GetNormalAtPoint(const TPoint<T>& pt) const override
	{
		return m_normal;
	}

	double FindIntersection(const TRay<T>& ray) override
	{
		const auto direction = ray.GetDirection();
		const auto a = direction.DotProduct(m_normal);
		if (a == 0)
		{
			// ray direction and normal of the plan are //
			return -1.0;
		}
		else
		{
			const TVect3D<T> originVec(ray.GetOrigin().GetX(), ray.GetOrigin().GetY(), ray.GetOrigin().GetZ());
			const double b = m_normal.DotProduct(originVec + (m_normal * m_distance).GetOpposite());
			return -1.0 * b / static_cast<double>(a);
		}
	}

	friend std::ostream& operator<<(std::ostream& os, TPlane<T> const& plane);

private:
	TVect3D<T> m_normal;
	T m_distance;
	Color m_color;
};

std::ostream& operator<<(std::ostream& os, TPlane<int> const& plane)
{
	os << "Normal : " << plane.m_normal << " - Distance : " << plane.m_distance << " - Color : " << plane.m_color;
	return os;
}

std::ostream& operator<<(std::ostream& os, TPlane<double> const& plane)
{
	os << "Normal : " << plane.m_normal << " - Distance : " << plane.m_distance << " - Color : " << plane.m_color;
	return os;
}