﻿/*
//my personnal main before the teacher main
#include <iostream> 

#include "TSphere.h"
#include "TRay.h"
#include "TVector.h"
#include "TList.h"
#include "TLight.h"
#include "TCamera.h"
#include "Quickhull.h"


using namespace std;

bool PointInPolygon(TPoint<int>& const point, TVector<TPoint<int>>& const polygon) {
	int i, j, polygonSize = polygon.Size();
	bool isInside = false;

	for (i = 0, j = polygonSize - 1; i < polygonSize; j = i++) {
		if (((polygon.Get(i).GetY() >= point.GetY()) != (polygon.Get(j).GetY() >= point.GetY())) &&
			(point.GetX() <= (polygon.Get(j).GetX() - polygon.Get(i).GetX()) * (point.GetY() - polygon.Get(i).GetY()) / (polygon.Get(j).GetY() - polygon.Get(i).GetY()) + polygon.Get(i).GetX())) {
			isInside = !isInside;
		}
	}
	return isInside;
}

int main() 
{
	TPoint<double> myPointA(3, 6, 54);
	TPoint<double> myPointB(7, 9, 8);
	TVect3D<double> myVect3D(myPointA, myPointB);
	TSphere<double> mySphere(myPointA, 3);
	TRay<double> myRay(myPointB, myVect3D);

	cout << "point A : " << myPointA << std::endl;
	cout << "point B : " << myPointB << std::endl;
	cout << "vector3D : " << myVect3D << std::endl;
	cout << "sphere : " << mySphere << std::endl;
	cout << "ray : " << myRay << std::endl;

	TList<int> list;
	list.PushBack(5);
	list.PushBack(3);
	list.PushBack(7);

	std::cout << "first list size : " << list.Size() << std::endl;
	std::cout << "first list elements : " << list << std::endl;

	list.Clear();
	list.PushBack(1);
	list.PushBack(2);
	list.PushBack(3);
	list.PushBack(4);
	list.PushBack(5);
	list.PushBack(6);
	list.PushBack(7);
	list.Insert(9, 5);
	list.PushFront(0);

	std::cout << "list size after clear and new operations : " << list.Size() << std::endl;
	std::cout << "list after clear and new operations : " << list << std::endl;

	TVect3D<double> myVect3DA(6, 9, 12);
	TVect3D<double> myVect3DB(62.3, 19, 12);
	TVect3D<double> myVect3DC(6.6, 93, 0);
	TVect3D<double> myVect3DD(5, 2.6, 1);
	TVect3D<double> myVect3DE(25, 28.6, 1);


	TVector<TVect3D<double>> vector;
	vector.PushBack(myVect3DA);
	vector.PushBack(myVect3DB);
	vector.PushBack(myVect3DC);
	vector.PushBack(myVect3DD);

	cout << "first vector size : " << vector.Size() << endl;
	cout << "first vector elements : " << vector << endl;

	vector.PushAt(myVect3DE, 1);
	vector.Pop();

	cout << "vector size after new operations : " << vector.Size() << endl;
	cout << "vector after new operations : " << vector << endl;


	//Test point inside polygon
	TPoint<int> pta(0, 0, 0);
	TPoint<int> ptb(10, 0, 0);
	TPoint<int> ptc(10, 10, 0);
	TPoint<int> ptd(0, 10, 0);

	TVector<TPoint<int>> polygonA;
	polygonA.PushBack(pta);
	polygonA.PushBack(ptb);
	polygonA.PushBack(ptc);
	polygonA.PushBack(ptd);

	TPoint<int> p(3, 5, 0);
	PointInPolygon(p, polygonA) ? cout << "Point inside polygon \n" : cout << "Point outside polygon \n";

	//The next part (Quickhull) is not only my code, I found the main functions at https://www.geeksforgeeks.org/quickhull-algorithm-convex-hull/
	// I updated it so it can work with my implementation of TPoint and TVector !
	TPoint<int> ptQuickHullA(0, 3, 0);
	TPoint<int> ptQuickHullB(1, 1, 0);
	TPoint<int> ptQuickHullC(2, 2, 0);
	TPoint<int> ptQuickHullD(4, 4, 0);
	TPoint<int> ptQuickHullE(0, 0, 0);
	TPoint<int> ptQuickHullF(1, 2, 0);
	TPoint<int> ptQuickHullG(3, 1, 0);
	TPoint<int> ptQuickHullH(3, 3, 0);
	TPoint<int> a[] = { ptQuickHullA, ptQuickHullB, ptQuickHullC, ptQuickHullD,
			ptQuickHullE, ptQuickHullF, ptQuickHullG, ptQuickHullH };
	int n = sizeof(a) / sizeof(a[0]);
	printHull(a, n);


	// Test light
	std::cout << " " << std::endl;
	std::cout << " ----------------------------" << std::endl;
	TPoint<int> posLight(0, 2, 4);
	Color colorLight(0, 255, 87, 0.5);
	Color colorLight2(255, 7, 95, 0.5);
	Color colorLight3 = colorLight + colorLight2;
	TLight<int> light(posLight, colorLight);

	std::cout << colorLight3 << std::endl;
	std::cout << light << std::endl;

	return 0;
}
*/


/*-------------------------------------------------------------------------------*/



#include "Bitmap.h"
#include "ImageTypes.h"
#include "TPoint.h"
#include "TVect3D.h"
#include "TCamera.h"
#include "TLight.h"
#include "TSphere.h"
#include "TPlane.h"
#include "Tools.h"
#include "ISource.h"
#include <vector>
#include <iostream>

Color GetColorAt(const TPoint<double>& _intersectionPosition,
	const TVect3D<double>& _incidentRayDirection,
	const std::vector<IObject<double>*>& _objects,
	const int& _nearestObjectIndex,
	const std::vector<ISource<double>*>& _lights,
	const double& _accuracy,
	const double& _ambientLight);

void ComputeReflection(const TPoint<double>& _intersectionPosition
	, const TVect3D<double>& _incidentRayDirection
	, const std::vector<IObject<double>*>& _objects
	, const std::vector<ISource<double>*>& _lights
	, const double& _accuracy
	, const double& _ambientLight
	, Color nearestObjectColor
	, TVect3D<double> nearestObjectNormal
	, Color& resultColor)
{
	// Algorithm to compute00 reflection
}

Color GetColorAt(const TPoint<double>& _intersectionPosition,
	const TVect3D<double>& _incidentRayDirection,
	const std::vector<IObject<double>*>& _objects,
	const int& _nearestObjectIndex,
	const std::vector<ISource<double>*>& _lights,
	const double& _accuracy,
	const double& _ambientLight)
{
	auto nearestObject = _objects[_nearestObjectIndex];
	const auto nearestObjectColor = nearestObject->GetColorAtPoint(_intersectionPosition);
	const auto nearestObjectNormal = nearestObject->GetNormalAtPoint(_intersectionPosition);

	auto resultColor = nearestObjectColor * _ambientLight;

	//ComputeReflection(_intersectionPosition
	//                  , _incidentRayDirection
	//                  , _objects
	//                  , _lights
	//                  , _accuracy
	//                  , _ambientLight
	//                  , nearestObjectColor
	//                  , nearestObjectNormal
	//                  , resultColor);

	for (auto& light : _lights)
	{
		const auto intersectToLightVec = TVect3D<double>(_intersectionPosition, light->GetPosition()).GetNormalized();
		const auto cosAlpha = nearestObjectNormal.DotProduct(intersectToLightVec);

		if (cosAlpha > 0)
		{
			auto isShadowed = false;
			const auto magnitude = intersectToLightVec.GetLength();

			TRay<double> shadowRay(_intersectionPosition, intersectToLightVec);
			std::vector<double> fromLightIntersections;

			for (auto& object : _objects)
				fromLightIntersections.push_back(object->FindIntersection(shadowRay));

			for (auto& intersection : fromLightIntersections)
			{
				if (intersection > _accuracy && intersection <= magnitude)
				{
					isShadowed = true;
					break;
				}
			}

			if (!isShadowed)
			{
				resultColor += (nearestObjectColor * light->GetColor()) * cosAlpha;

				if (nearestObjectColor.Alpha() > 0.0 && nearestObjectColor.Alpha() < 1.0)
				{
					// TODO 5) Write algorithm here !!!
					// TODO 6) Build and run your code to generate rendering

					double ratio = nearestObjectNormal.DotProduct(_incidentRayDirection.GetOpposite());
					TVect3D<double> reflectedRay = nearestObjectNormal * ratio + _incidentRayDirection;
					TVect3D<double> upscale = reflectedRay * 2;
					TVect3D<double> finalReflectedRay = (upscale - _incidentRayDirection).GetNormalized();
					double specularRatio = finalReflectedRay.DotProduct(intersectToLightVec);
					if (specularRatio > 0.0)
					{
						specularRatio = std::pow(specularRatio, 10);
						resultColor += (light->GetColor() * (specularRatio * nearestObjectColor.Alpha()));
					}
				}
			}
		}
	}

	return resultColor.Clip();
}

int main()
{
	static const auto width = 1280;
	static const auto height = 960;
	static const auto size = width * height;
	static const auto ratio = static_cast<double>(width) / static_cast<double>(height);
	static const auto ambientLight = 0.2;
	static const auto accuracy = 0.00000001;
	static const auto dpi = 75;
	static const TPoint<double> origin(0., 0., 0.);
	static const TVect3D<double> xAxisVector(1.0, 0.0, 0.0);
	static const TVect3D<double> yAxisVector(0.0, 1.0, 0.0);
	static const TVect3D<double> zAxisVector(0.0, 0.0, 1.0);

	Bitmap image(width, height, dpi);
	const auto pData = new TRgb<double>[size];

	static const TPoint<double> cameraPosition(3.0, 1.5, -4.0);
	static const TVect3D<double> lookAt(0.0, 0.0, 0.0);
	const auto diffBtw = TVect3D<double>(cameraPosition.GetX(), cameraPosition.GetY(), cameraPosition.GetZ()) - lookAt;
	const auto cameraDirection = diffBtw.GetOpposite().GetNormalized();
	const auto cameraRight = (yAxisVector * cameraDirection).GetNormalized();
	const auto cameraDown = cameraRight * cameraDirection;

	TCamera<double> cameraScene(cameraPosition, cameraDirection, cameraRight, cameraDown);

	static const Color white(1.0, 1.0, 1.0, 0.0);
	static const Color green(0.5, 1.0, 0.5, 0.3);
	static const Color red(1.0, 0.5, 0.5, 0.3);
	static const Color tile(1.0, 1.0, 1.0, 2.0);

	static const TPoint<double> lightPosition(-7.0, 10.0, -10.0);
	TLight<double> sceneLight(lightPosition, white);

	std::vector<ISource<double>*> lights;
	lights.push_back(&sceneLight);

	TSphere<double> sphere(origin, 1.0, green);
	TSphere<double> sphereTwo(TPoint<double>(2.0, -0.5, 0.0), 0.5, red);
	TPlane<double> plane(yAxisVector, -1.0, tile);

	std::vector<IObject<double>*> objects;
	objects.push_back(&sphere);
	objects.push_back(&sphereTwo);
	objects.push_back(&plane);

	double xStep;
	double yStep;
	int currentPosition;

	for (auto x = 0; x < width; ++x)
	{
		for (auto y = 0; y < height; ++y)
		{
			currentPosition = y * width + x;

			if constexpr (width > height)
			{
				xStep = ((x + 0.5) / width) * ratio - ((width - height) / static_cast<double>(height)) / 2.0;
				yStep = ((height - y) + 0.5) / height;
			}
			else if (width < height)
			{
				xStep = (x + 0.5) / width;
				yStep = (((height - y) + 0.5) / height) / ratio - (((height - width) / static_cast<double>(width)) / 2.0);
			}
			else
			{
				xStep = (x + 0.5) / width;
				yStep = ((height - y) + 0.5) / height;
			}

			const auto cameraRayOrigin = cameraScene.GetPosition();
			const auto cameraRayDirection = (cameraScene.GetDirection() + (cameraRight * (xStep - 0.5)) + (cameraDown * (yStep - 0.5))).GetNormalized();

			TRay<double> cameraRay(cameraRayOrigin, cameraRayDirection);

			std::vector<double> intersections;

			for (auto object : objects)
				intersections.push_back(object->FindIntersection(cameraRay));

			auto idx = Tools::FindNearestIntersection(intersections);

			if (idx == -1)
			{
				pData[currentPosition].r = 0.0;
				pData[currentPosition].g = 0.0;
				pData[currentPosition].b = 0.0;
			}
			else
			{
				if (auto k = intersections[idx]; k > accuracy)
				{
					const auto intersectionPosition = cameraRayOrigin + (k * cameraRayDirection);

					// TODO 2) Build and run the code and you will have the first render
					// with just colors without lights. The output file is in the build directory named ResultRayTracer.bmp
					// TODO 3) next line must be commented, it’s just to test the first rendering
					//const auto resultColor = objects[idx]->GetColorAtPoint(intersectionPosition);

					// TODO 4) The next line must be uncommented
					const auto resultColor = GetColorAt(intersectionPosition
				                                        , cameraRay.GetDirection()
				                                        , objects
				                                        , idx
				                                        , lights
				                                        , accuracy
				                                        , ambientLight);

					pData[currentPosition].r = resultColor.Red();
					pData[currentPosition].g = resultColor.Green();
					pData[currentPosition].b = resultColor.Blue();
				}
			}
		}
	}

	image.SetData(pData, size);
	image.Save("ResultRayTracer.bmp");
	return 0;
}
